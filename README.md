# Overview

Django project to demonstrate test driven development using REST FRAMEWORK and Django's built in unit tests applied on a Postgresql database

## Installation

From the command line

``` 
$ git clone the repository
$ pipenv shell
$ pip install -r requirements.txt

```

## Testing

To run the tests from the command line

``` 
$ cd kidshows
$ python manage.py test

```

## Output

```
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
..........
----------------------------------------------------------------------
Ran 10 tests in 0.241s

OK

```
