from django.db import models

# Create your models here.


class Show(models.Model):
    """Show Model
    Defines the attributes of the show"""
    name = models.CharField(max_length=255)
    director = models.CharField(max_length=255)
    main_character = models.CharField(max_length=255)
    channel = models.CharField(max_length=255)

    def get_show(self):
        return self.name + ' is directed by ' + self.director + ' on ' + self.channel + ' starring ' + self.main_character

    def __repr__(self):
        return self.name
