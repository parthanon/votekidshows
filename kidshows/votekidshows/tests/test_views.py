import json
from rest_framework import status
from ..models import Show
from ..serializers import ShowSerializer
from django.test import TestCase, Client
from django.urls import reverse

# initialize the APIclient app
client = Client()


class GetAllShowsTest(TestCase):
    """Test module to get all the shows API"""

    def setUp(self):
        Show.objects.create(name="Casper", director="Casp",
                            main_character="Casper", channel="CaspTV")
        Show.objects.create(name="Muffin", director="Muff",
                            main_character="Muffin", channel="MTV")
        Show.objects.create(name="Ruskin", director="George Bailey",
                            main_character="Roger Harper", channel="ZTV")

    def test_get_all_shows(self):
        # get API response
        response = client.get(reverse('get_add_show'))

        # get data from database

        shows = Show.objects.all()
        serializer = ShowSerializer(shows, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleShow(TestCase):
    def setUp(self):
        self.casper = Show.objects.create(name="Casper", director="Casp",
                                          main_character="Casper", channel="CaspTV")
        self.muffin = Show.objects.create(name="Muffin", director="Muff",
                                          main_character="Muffin", channel="MTV")
        self.ruskin = Show.objects.create(name="Ruskin", director="George Bailey",
                                          main_character="Roger Harper", channel="ZTV")

    def test_get_valid_single_show(self):
        response = client.get(
            reverse('get_delete_update_show', kwargs={'pk': self.ruskin.pk}))
        show = Show.objects.get(pk=self.ruskin.pk)
        serializer = ShowSerializer(show)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_show(self):
        response = client.get(
            reverse('get_delete_update_show', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewShowTest(TestCase):
    """Test module to insert a new show"""

    def setUp(self):
        self.valid_payload = {
            'name': 'Muffin',
            'director': 'Muff',
            'main_character': 'Muffin',
            'channel': 'WhiteTV'
        }

        self.invalid_payload = {
            'name': '',
            'director': 'Rujvinder',
            'main_character': 'Maninder',
            'channel': 'EmTV'
        }

    def test_create_valid_show(self):
        response = client.post(
            reverse('get_add_show'),
            data=json.dumps(self.valid_payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_show(self):
        response = client.post(reverse('get_add_show'),
                               data=json.dumps(self.invalid_payload),
                               content_type='application/json',)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleShowTest(TestCase):
    """Test module for updating an existing show record"""

    def setUp(self):
        self.casper = Show.objects.create(name="Casper", director="Casp",
                                          main_character="Casper", channel="CaspTV")
        self.muffin = Show.objects.create(name="Muffin", director="Muff",
                                          main_character="Muffin", channel="MTV")
        self.ruskin = Show.objects.create(name="Ruskin", director="George Bailey",
                                          main_character="Roger Harper", channel="ZTV")

        self.valid_payload = {
            'name': 'Muffin',
            'director': 'Muff',
            'main_character': 'Muffin',
            'channel': 'WhiteTV'
        }

        self.invalid_payload = {
            'name': '',
            'director': 'Rujvinder',
            'main_character': 'Maninder',
            'channel': 'EmTV'
        }

    def test_update_valid_show(self):
        response = client.put(
            reverse('get_delete_update_show', kwargs={'pk': self.ruskin.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_invalid_show(self):
        response = client.put(
            reverse('get_delete_update_show', kwargs={'pk': self.ruskin.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleShowTest(TestCase):
    def setUp(self):
        self.casper = Show.objects.create(name="Casper", director="Casp",
                                          main_character="Casper", channel="CaspTV")
        self.muffin = Show.objects.create(name="Muffin", director="Muff",
                                          main_character="Muffin", channel="MTV")
        self.ruskin = Show.objects.create(name="Ruskin", director="George Bailey",
                                          main_character="Roger Harper", channel="ZTV")

    def test_valid_delete_show(self):
        response = client.delete(
            reverse('get_delete_update_show', kwargs={'pk': self.muffin.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_puppy(self):
        response = client.delete(
            reverse('get_delete_update_show', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
