from django.urls import path
from . import views

urlpatterns = [

    path('api/v1/shows/<pk>', views.get_delete_update_show,
         name="get_delete_update_show"),
    path('api/v1/shows', views.get_add_show, name="get_add_show")
]
