from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Show
from .serializers import ShowSerializer

# Create your views here.
@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_show(request, pk):
    try:
        show = Show.objects.get(pk=pk)
    except Show.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # get details of a single show
    if request.method == 'GET':
        serializer = ShowSerializer(show)
        return Response(serializer.data)

   # delete a single show
    if request.method == 'DELETE':
        show.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

   # update a single show
    if request.method == 'PUT':
        serializer = ShowSerializer(show, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def get_add_show(request):
    # get all shows
    if request.method == 'GET':
        shows = Show.objects.all()
        serializer = ShowSerializer(shows, many=True)
        return Response(serializer.data)
    # insert new post
    elif request.method == 'POST':
        data = {
            'name': request.data.get('name'),
            'director': request.data.get('director'),
            'main_character': request.data.get('main_character'),
            'channel': request.data.get('channel')

        }
        serializer = ShowSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
